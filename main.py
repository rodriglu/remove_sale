import shopify
import csv
import math

# shopify connect
shop_url = "https://%s:%s@gitz-office.myshopify.com/admin" % ("8da4d55a17e5f6604c39867e0691520c", "b96c06b0ac20525d2b25bb415934385c")
shopify.ShopifyResource.set_site(shop_url)
shop = shopify.Shop.current()

# Returns a product given a handle 
def get_product(h):
	products = shopify.Product()
	if(products.find(handle=h)):
		temp = products.find(handle=h)
		for v in temp:
			id = v.id
			print "Working: " + v.handle
		product = shopify.Product.find(id)
		return product
	else:
		return None

# add_sale tag to products 
def add_sale(product):
	product = get_product(product)
	if product:
		if "Sale" not in product.tags:
			product.tags += ",Sale"
			product.save()

# scan through all products and remove sale tag if present
def remove_sale():
	products_count = shopify.Product.count(published_status='published')
	limit = 50
	pages = int(math.ceil(products_count/limit))
	for i in range(1, pages + 1):
		products = shopify.Product.find(limit=limit, page=i, published_status='published')
		for product in products:
			print product.tags 
			if product:
				if "Sale," in product.tags:
					product.tags = product.tags.replace("Sale,", "")
					product.save()
				elif ", Sale" in product.tags:
					product.tags = product.tags.replace(", Sale", "")
					product.save()
				elif "Sale" in product.tags:
					product.tags = product.tags.replace("Sale", "")
					product.save()


	

def main():
	# iterate over csv file
	with open('sale.csv') as csvfile:
		readCSV = csv.reader(csvfile, delimiter=',')
		for row in readCSV:
			if row[0] == "PartNumber":
				continue
			add_sale(row[0])

main()


